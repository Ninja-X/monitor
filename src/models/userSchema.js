
const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const uniqid = require('uniqid');
var randomstring = require("randomstring");
const mongoosePaginate = require('mongoose-paginate-v2');
var shajs = require('sha.js')
 

const userSchema = mongoose.Schema({
        name: {
            type: String,
            required: true
         
        },
       token: {
           type: String,
           required: false

     },
        profile_score : {
            type: Number,
            required: false
        },
         
        daily_stats : {
            type: Object,
            required:false
        }         
        ,
        recent_feeds : {
            type: Object,
            default: {}
        },

        reset_code: {
          type: String,
          default: "lyuinkniukgyukguuy67@#"
        },
        
         transactions: [] ,

         budget_stats: {
            active: [],
            history: []
           },
     
       
        devices : [],
        phone_number: {
            type: String,
            required: false
         
        },
        address:{
            type: String,
            required: false
        },
        sex:{
            type: String,
            required: false
        },
        nationality:{
            type: String,
            required: false
        },
        birth_date:{
            type: String,
            required: false
        },
        budgets: [],
        defaults: [],
        check_in_counts : {
            type: Number,
            default : 0
        },
        fin_id: {
        
            type: String,
            required: false
        } ,
        email: {
            type: String,
            required: true,
            lowercase: true,
            index: true
        },
        password: {
            type: String,
            required: true,
            minLength: 8
        },
      
        time_zone: {
            type: String,
            required: false
         
        },
       reminder_settings:{
           type: Object,
           default: {count: 1, active: false}
       },
        achievements_tracker: {
            last_checked : {
             type: String,
             default: ""
            },
            max_streak: {
                type: Number,
                default: 0,
            },
            current_streak: {
                type: Number,
                default: 0
            },
            lock: {
                type: Boolean,
                default: false
            },
            congratulate: {
                type:Boolean,
                 default: false
            }
        },
        notifications : [],
        rewards_history : [],
        location_history : [],
        points : {
            type: Number,
            default: 0
        },
        verified_email : {
            type: Boolean,
            default: false
        },
        verification_link :{
            type: String,
            default : ""
        },

        article_counts :{
            type: Number,
            default: 0
        },

        video_counts :{
            type: Number,
            default: 0
        },

        version: {
           type: String,
           required: false
        },
        podcasts_counts :{
            type: Number,
            default: 0
        }


    
        
    })
    
     
     userSchema.plugin(mongoosePaginate);

    userSchema.pre('save', async function (next) {
        // Hash the password before saving the user model
        const user = this

        
        if (user.isModified('password')) {
            user.password = await bcrypt.hash(user.password, 8)
        }
        next()
    })

    userSchema.methods.generateUUID =  async function() {
        // Generate an auth token for the user
       const user = this;
       user.fin_id ='fin-'+randomstring.generate({
        length: 12,
        charset: 'numeric'
      });
    

    }

    userSchema.methods.updatePassword =  async function(pass) {
        // Generate an auth token for the user
       const user = this;
       user.password = await bcrypt.hash(pass, 8)
    

    }


    userSchema.methods.generateVerificationLink = async function(){
        const user = this;
        user.verification_link = shajs('sha256').update('42').digest('hex')
    }

    userSchema.methods.generateAuthToken = async function() {
        // Generate an auth token for the user
        const user = this
        const token = jwt.sign({_id: user._id}, process.env.JWT_SECRET || "SECRETBOY",{expiresIn : 60 * 60 * 24 * 4})
        console.log(token)
        user.token = token;
        await user.save() 
        return token
    }

    userSchema.methods.addGoal = async function() {
        // Generate an auth token for the user
        const user = this
      
        await user.save() 
    //  
      return token
    }

    userSchema.methods.generateResetCode = async function() {
  
        // Generate an auth token for the user
        const user = this
        user.reset_code = randomstring.generate({
            length: 6,
            charset: 'numeric'
          });
        await user.save() 
        return user.reset_code
      }
      
    

    userSchema.methods.addExpenses = async function() {
      // Generate an auth token for the user
      const user = this
      
      await user.save() 
  //    return token
    }
    
    userSchema.methods.addDevice= async function(token,uuid) {
  
      // Generate an auth token for the user
      const user = this
      user.devices.push({push_token: token, uuid: uuid })
      user.markModified('devices');
      await user.save() 
  //    return token
    }
    
    userSchema.methods.yz= async function() {
        // Generate an auth token for the user
        const user = this
        const token = jwt.sign({_id: user._id}, "SECRETBOY",{expiresIn : 60})
        console.log(token)
        user.token = token;
        await user.save() 
        return token
    }
    
    
    userSchema.methods.authenticate = async (email,password) => {
        // Search for a user by email and password.
        console.log(email,password)
        if(typeof password == "undefined" || typeof email == "undefined"){
            return false
           // console.log("Password undefined"
        }
        const user = await User.findOne({ email} )
      
        const isPasswordMatch = await bcrypt.compare(password, user.password)
        if (!isPasswordMatch) {
           return false;
        }
        return true;

    }
    
    
    const User = mongoose.model('User', userSchema)    
     User.paginate().then({}) // Usage
    module.exports = User