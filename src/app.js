// require('./config/database')
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
var Redis = require("ioredis");
var client_bus = new Redis(process.env.REDIS_URI);
var shell = require('shelljs');
const Queue = require('bull')
require('dotenv').config()
app.use(bodyParser.json());
var api = express.Router();
app.use('/api',api)
api.post("/jenkins/webhook", (req,res) => {
console.log(req.body)
console.log(req.query)
var service_name = req.query.image.split("/").slice(-1)[0].split(":")[0]
console.log("Logging to ECR...")
if (shell.exec(`export AWS_ACCESS_KEY_ID=${process.env.AWS_ACCESS_KEY_ID} && export AWS_SECRET_ACCESS_KEY=${process.env.AWS_SECRET_ACCESS_KEY} $(aws ecr get-login --region us-east-1 --no-include-email)`).code !== 0) {
    shell.echo('Error: Docker Login failed');
    shell.exit(1);
  }
console.log(`Pulling Image ${req.query.image} from ECR...`)
if (shell.exec(`docker pull ${req.query.image}`).code !== 0) {
    shell.echo('Error: Docker Image failed');
    shell.exit(1);
  }
  console.log("Preparing for deployment: Shutting down service...")
  if (shell.exec(`cd .././finllect && docker-compose -f docker-compose-ecr.yml kill ${service_name}`).code !== 0) {
    shell.echo('Error: Finllect not found or could not kill service: '+service_name);
    shell.exit(1);
  }
  console.log("Deploying Latest Image to Container...")
  if (shell.exec(`cd && cd finllect && docker-compose -f docker-compose-ecr.yml up -d --no-deps ${service_name}`).code !== 0) {
    shell.echo('Error: "Failed to Deploy service: '+service_name);
    shell.exit(1);
  }else{
    console.log("Successful Deployment")
  }
  
res.json({msg: "Successful Deployment"})
})

api.post("/jenkins-whatsdown/webhook", (req,res) => {
  console.log(req.body)
  console.log(req.query)
  var service_name = req.query.image.split("/").slice(-1)[0].split(":")[0]
  console.log("Logging to ECR...")
  if (shell.exec(`export AWS_ACCESS_KEY_ID=${process.env.AWS_ACCESS_KEY_ID} && export AWS_SECRET_ACCESS_KEY=${process.env.AWS_SECRET_ACCESS_KEY} $(aws ecr get-login --region us-east-1 --no-include-email)`).code !== 0) {
      shell.echo('Error: Docker Login failed');
      shell.exit(1);
    }
  console.log(`Pulling Image ${req.query.image} from ECR...`)
  if (shell.exec(`docker pull ${req.query.image}`).code !== 0) {
      shell.echo('Error: Docker Image failed');
      shell.exit(1);
    }
    console.log("Preparing for deployment: Shutting down service...")
    if (shell.exec(`cd .././whatsdown && docker-compose -f docker-compose-ecr.yml kill ${service_name}`).code !== 0) {
      shell.echo('Error: Finllect not found or could not kill service: '+service_name);
      shell.exit(1);
    }
    console.log("Deploying Latest Image to Container...")
    if (shell.exec(`cd && cd whatsdown && docker-compose -f docker-compose-ecr.yml up -d --no-deps ${service_name}`).code !== 0) {
      shell.echo('Error: "Failed to Deploy service: '+service_name);
      shell.exit(1);
    }else{
      console.log("Successful Deployment")
    }
    
  res.json({msg: "Successful Deployment"})
  })

api.get("/docker/shutdown", (req,res) => {
  console.log(req.body)
  res.json({msg: "Hello Server main"})
})
  
api.get("/docker/startup", (req,res) => {
  console.log(req.body)
  res.json({msg: "Hello Server main"})
})
  


// docker-compose up -d --no-deps <service_name>

app.get("/*", (req,res) => {

    res.json({msg: "Hello Server 404"})
})





module.exports = app;